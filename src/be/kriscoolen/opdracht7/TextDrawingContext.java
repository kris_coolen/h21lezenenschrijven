package be.kriscoolen.opdracht7;

public class TextDrawingContext implements DrawingContext {
    @Override
    public void draw(Rectangle rect) {
        System.out.println(rect);
    }

    @Override
    public void draw(Circle circle) {
        System.out.println(circle);
    }

    @Override
    public void draw(Triangle triangle) {
        System.out.println(triangle);
    }
}
