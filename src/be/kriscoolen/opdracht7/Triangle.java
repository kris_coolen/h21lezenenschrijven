package be.kriscoolen.opdracht7;

import java.util.Formatter;

public class Triangle extends Shape {
    private static int count = 0;
    protected int width;
    protected int height;
    protected int perpendicular;

    public Triangle(){
        this(0, 0, 0, 0, 0);
    }

    public Triangle(int width, int height, int perpendicular){
        this(width, height, perpendicular, 0 , 0);
    }

    public Triangle(int width, int height, int perpendicular, int x, int y){
        count++;
        setWidth(width);
        setHeight(height);
        setPerpendicular(perpendicular);
        setPosition(x, y);
    }

    public Triangle(Triangle t){
        this(t.width, t.height, t.perpendicular, t.x, t.y);
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width<0?-width:width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height<0?-height:height;
    }

    public int getPerpendicular() {
        return perpendicular;
    }

    public void setPerpendicular(int perpendicular) {
        this.perpendicular = perpendicular<0?-perpendicular:perpendicular;
    }

    @Override
    public double getArea() {
        return width*height/2;
    }

    @Override
    public double getPerimeter() {
        int temp = width - perpendicular;
        return Math.sqrt(perpendicular * perpendicular + height * height)
                + Math.sqrt(temp * temp + height * height);
    }

    @Override
    public void grow(int d) {
        setWidth(width+d);
        setHeight(height+d);
    }

    @Override
    public String toString() {
        Formatter f = new Formatter().format("Triangle:");
        f.format("\n\tWidth: %d\n\tHeight: %d\n\tPerpendicular %d", width, height, perpendicular);
        f.format("\n\tPosX: %d\n\tPosY: %d", x, y);
        return f.toString();
    }

    @Override
    public void draw(DrawingContext dc) {
        dc.draw(this);
    }

    @Override
    public void scale(int factor) {
        double f = (double)factor/100;
        setWidth((int)(width*f));
        setHeight((int)(height*f));
        setPerpendicular((int)(perpendicular*f));
    }

    public static int getCount(){
        return count;
    }
}
