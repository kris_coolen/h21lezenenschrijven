package be.kriscoolen.opdracht7;

import java.util.Formatter;

public class Square extends Rectangle {
    private static int count = 0;

    public Square() {
        this(0, 0, 0);
    }

    public Square(int side) {
        this(side, 0, 0);
    }

    public Square(int side, int x, int y) {
        count++;
        setSide(side);
        setPosition(x, y);
    }

    public Square(Square s) {
        this(s.getSide(), s.getX(), s.getY());
    }

    public void setSide(int side) {
        setWidth(side);
        setHeight(side);
    }

    public int getSide() {
        return getWidth();
    }

    public static int getCount() {
        return count;
    }

    @Override
    public double getArea() {
        int w = getWidth();
        return w * w;
    }

    @Override
    public double getPerimeter() {
        int w = getWidth();
        return w * 4;
    }

    @Override
    public void grow(int d) {
        setSide(getWidth() + d);
    }

    @Override
    public void draw(DrawingContext dc) {
        dc.draw(this);
    }

    @Override
    public String toString() {
        Formatter f = new Formatter().format("Square:");
        f.format("\n\tRadius: %d", getSide());
        f.format("\n\tPosX: %d\n\tPosY: %d", x, y);
        return f.toString();
    }

    @Override
    public void scale(int factor) {
        setSide((int) ((double) factor / 100 * getSide()));
    }
}
