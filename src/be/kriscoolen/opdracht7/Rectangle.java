package be.kriscoolen.opdracht7;

import java.util.Formatter;

public class Rectangle extends Shape {
    private static int count = 0;
    protected int width;
    protected int height;

    public Rectangle() {
        this(0, 0, 0, 0);
    }

    public Rectangle(int width, int height){
        this(width, height, 0, 0);
    }

    public Rectangle(int width, int height, int x, int y) {
        count++;
        setWidth(width);
        setHeight(height);
        setX(x);
        setY(y);
    }

    public Rectangle( Rectangle r ) {
        this(r.width, r.height, r.x, r.y);
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width < 0 ? -width : width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height < 0 ? -height : height;
    }

    @Override
    public double getArea() {
        return width * height;
    }

    @Override
    public double getPerimeter() {
        return (width + height) * 2;
    }

    @Override
    public void grow(int d) {
        setWidth(width + d);
        setHeight(height + d);
    }

    @Override
    public void draw(DrawingContext dc) {
        dc.draw(this);
    }

    @Override
    public String toString(){
        Formatter f = new Formatter().format("Rectangle:");
        f.format("\n\tWidth: %d\n\tHeight: %d", width, height);
        f.format("\n\tPosX: %d\n\tPosY: %d", x, y);
        return f.toString();
    }

    @Override
    public void scale(int factor) {
        double f = (double)factor / 100;
        setWidth((int)(width*f));
        setHeight((int)(height*f));
    }

    public static int getCount(){
        return count;
    }
}
