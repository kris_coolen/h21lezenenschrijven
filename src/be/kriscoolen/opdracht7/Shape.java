package be.kriscoolen.opdracht7;

public abstract class Shape implements Drawable {
    private static int count = 0;
    protected int x;
    protected int y;

    public Shape(){
        this(0, 0);
    }
    public Shape(int x, int y){
        count++;
        setX(x);
        setY(y);
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setPosition(int x, int y){
       setX(x);
       setY(y);
    }

    public abstract double getArea();

    public abstract double getPerimeter();

    public void grow(int d){

    }

    public static int getCount(){
        return count;
    }
}
