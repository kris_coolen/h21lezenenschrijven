package be.kriscoolen.opdracht7;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class WriteDrawing {
    public static void main(String[] args) {

        Drawing drawing = new Drawing();
        Rectangle rect = new Rectangle(10, 10,1,2);
        Square squa = new Square(40,11,12);
        Triangle tria = new Triangle(10, 20, 30,21,22);
        Circle circ = new Circle(20,31,32);

        drawing.add(rect);
        drawing.add(tria);
        drawing.add(squa);
        drawing.add(circ);
        drawing.scaleDouble();

        //schrijf deze dwrawing naar een bestand genaamd figuren.txt
        try(FileOutputStream file = new FileOutputStream("figuren.txt");
            ObjectOutputStream out = new ObjectOutputStream(file);){
            out.writeObject(drawing);
        }catch(IOException ex){
            System.out.println(ex.getMessage());
        }





    }
}
