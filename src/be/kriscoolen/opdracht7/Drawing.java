package be.kriscoolen.opdracht7;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Formatter;
import java.util.Iterator;

public class Drawing implements Drawable,Iterable<Drawable>{

    //public Drawable[] list = new Drawable[MAX_SIZE];
    public Collection<Drawable> list = new ArrayList<>();

    public void add(Drawable d){
        list.add(d);
    }

    public void remove(Drawable d){
        list.remove(d);
    }

    public void clear(){
        list.clear();
    }

    @Override
    public void draw(DrawingContext dc) {
        System.out.println("Drawing: ");
        for (Drawable d : list){
            if(d!=null){
                d.draw(dc);
                //s.draw();
            }
        }
    }

    @Override
    public String toString() {
        Formatter f = new Formatter();
        f.format("Drawing: %d", list.size() );
        int i=0;
        for(Drawable d: list){
            f.format("\n %d>%s",++i,d.toString());
        }
        return f.toString();
    }

    @Override
    public void scale(int factor) {
        for(Drawable d: list){
            d.scale(factor);
        }
    }

    @Override
    public Iterator<Drawable> iterator() {
        return list.iterator();
    }
}
