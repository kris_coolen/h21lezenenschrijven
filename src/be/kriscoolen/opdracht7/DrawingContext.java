package be.kriscoolen.opdracht7;

public interface DrawingContext {
    public void draw(Rectangle rect);
    public void draw(Circle circle);
    public void draw(Triangle triangle);
}
