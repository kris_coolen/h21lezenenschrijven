package be.kriscoolen.opdracht7;

import java.io.FileInputStream;
import java.io.ObjectInputStream;

public class ReadDrawingAndPrint {
    public static void main(String[] args) {
        try(FileInputStream file = new FileInputStream("figuren.txt");
            ObjectInputStream in = new ObjectInputStream(file);){
            Drawing drawing = (Drawing) in.readObject();
            System.out.println(drawing);
        }catch(Exception ex){
            System.out.println(ex.getMessage());
        }
    }

}
