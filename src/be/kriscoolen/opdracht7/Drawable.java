package be.kriscoolen.opdracht7;

public interface Drawable extends Scalable {
    public void draw(DrawingContext dc);
}
