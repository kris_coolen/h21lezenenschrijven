package be.kriscoolen.opdracht7;

import java.io.Serializable;

public interface Scalable extends Serializable {
    public static final int QUARTER = 25;
    public static final int HALF = 50;
    public static final int DOUBLE = 200;

    public abstract void scale(int factor);

    public default void scaleDouble() {
        scale(DOUBLE);
    }

    public default void scaleHalf(){
        scale(HALF);
    }
}
