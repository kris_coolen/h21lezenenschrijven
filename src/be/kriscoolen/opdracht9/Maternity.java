package be.kriscoolen.opdracht9;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.time.LocalDate;
import java.time.Month;

public class Maternity {

    public static void main(String[] args) throws IOException {
        LocalDate date = LocalDate.of(1985, Month.FEBRUARY,13);
        Person p = new Person("Kris","Coolen",date);
        String fileName = p.getFirstName()+"_"+p.getLastName()+".txt";
        try(FileOutputStream file = new FileOutputStream(fileName);
            ObjectOutputStream out = new ObjectOutputStream(file);){
            out.writeObject(p);
            System.in.read();
        }catch(IOException ex){
            System.out.println("ik zit in de catch");
            System.out.println(ex.getMessage());
            ex.printStackTrace();
        }


    }
}
