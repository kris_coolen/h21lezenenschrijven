package be.kriscoolen.opdracht2;

import java.io.*;


public class WriteFile {
    public static void main(String[] args) throws IOException{

        try(FileWriter file=new FileWriter("MyFile2.txt",true);){
            file.write("TITEL\nDit is een stuk tekst. Ik zit maar wat te brabbelen.\n");
            file.write("Dit is een tweede alinea in het stuk tekst. Blablabla\nBlablablabla\n\n");
        }catch(IOException ex){
            System.out.println("Ooops, something went wrong");
            System.out.println(ex.getMessage());
        }
    }
}
