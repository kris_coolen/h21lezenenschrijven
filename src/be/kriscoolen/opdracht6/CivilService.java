package be.kriscoolen.opdracht6;

import java.io.FileInputStream;
import java.io.ObjectInputStream;

public class CivilService {
    public static void main(String[] args) {
        try(FileInputStream file = new FileInputStream("Kris_coolen.ser");
            ObjectInputStream in = new ObjectInputStream(file);){

            Person p = (Person) in.readObject();
            System.out.println(p);

        }catch(Exception ex){
            System.out.println(ex.getMessage());
        }
    }
}
