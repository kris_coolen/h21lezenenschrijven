package be.kriscoolen.opdracht6;

import java.io.*;
import java.time.LocalDate;
import java.time.Month;

public class Maternity {

    public static void main(String[] args) {
        LocalDate date = LocalDate.of(1985, Month.FEBRUARY,13);
        Person p = new Person("Kris","Coolen",date);
        String fileName = p.getFirstName()+"_"+p.getLastName()+".txt";
        try(FileOutputStream file = new FileOutputStream(fileName);
            ObjectOutputStream out = new ObjectOutputStream(file);){
            out.writeObject(p);
        }catch(IOException ex){
            System.out.println(ex.getMessage());
        }

    }
}
