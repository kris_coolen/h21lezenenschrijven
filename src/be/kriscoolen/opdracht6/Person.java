package be.kriscoolen.opdracht6;

import java.io.Serializable;
import java.time.LocalDate;

public class Person implements Serializable {

    private String firstName;
    private String lastName;
    private LocalDate localDate;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public LocalDate getLocalDate() {
        return localDate;
    }

    public void setLocalDate(LocalDate localDate) {
        this.localDate = localDate;
    }

    public Person(String firstName, String lastName, LocalDate localDate) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.localDate = localDate;
    }

    @Override
    public String toString() {
        return "Person{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", localDate=" + localDate +
                '}';
    }
}
