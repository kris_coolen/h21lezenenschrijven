package be.kriscoolen.opdracht3;
import java.io.*;
public class ReadFile {
    public static void main(String[] args) {
        try(FileReader file=new FileReader("CV_VeraPaumen.txt")){
            int character;
            while((character=file.read())!=-1){
                System.out.print((char) character);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
