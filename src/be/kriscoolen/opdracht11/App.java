package be.kriscoolen.opdracht11;

import java.io.IOException;

public class App {

    public static void main(String[] args) {

        ApplicationProperties appProps = new ApplicationProperties("Application.properties");
        try {
            //appProps.store();
            appProps.load();
            System.out.println(appProps);
        }catch(IOException io){
            io.printStackTrace();
        }

    }

}
