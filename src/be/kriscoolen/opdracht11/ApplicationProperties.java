package be.kriscoolen.opdracht11;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public class ApplicationProperties {
    private String fileName;
    Properties props = new Properties();

    public ApplicationProperties(String file){
        this.fileName = file;
        this.setX(100);
        this.setY(10);
        this.setWidth(5);
        this.setHeight(8);

    }

    public void setX(int x){
        props.setProperty("x", String.valueOf(x));
    }
    public int getX(){
        return Integer.parseInt(props.getProperty("x"));
    }

    public void setY(int y){
        props.setProperty("y",String.valueOf(y));

    }

    public int getY(){
        return Integer.parseInt(props.getProperty("y"));
    }

    public void setWidth(int w){
        props.setProperty("width",String.valueOf(w));
    }

    public int getWidth(){
        return Integer.parseInt(props.getProperty("width"));
    }

    public void setHeight(int h){
        props.setProperty("height",String.valueOf(h));
    }

    public int getHeight(){
        return Integer.parseInt(props.getProperty("height"));
    }

    public void load() throws IOException {
        props.load(new FileInputStream(fileName));
    }

    public void store() throws IOException{
        props.store(new FileOutputStream(fileName),"Application properties");
    }

    @Override
    public String toString() {
        return "ApplicationProperties{" +
                "fileName='" + fileName + '\'' +
                ", props=" + props +
                '}';
    }
}
