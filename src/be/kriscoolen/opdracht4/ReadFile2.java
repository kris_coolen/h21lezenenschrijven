package be.kriscoolen.opdracht4;
import java.io.*;
import java.nio.file.*;

public class ReadFile2 {
    public static void main(String[] args) {
        Path path = Paths.get("CV_VeraPaumen.txt");
        try(BufferedReader reader = Files.newBufferedReader(path)){
            String line = null;
            while((line=reader.readLine())!=null){
                System.out.println(line);
            }

        } catch (IOException e) {
            System.out.println("Oops, something went wrong!");
            System.out.println(e.getMessage());
            System.out.println(e.getStackTrace());
        }
    }
}
