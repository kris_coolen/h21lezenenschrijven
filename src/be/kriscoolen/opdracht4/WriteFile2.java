package be.kriscoolen.opdracht4;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.*;

public class WriteFile2 {

    public static void main(String[] args){

    Path path = Paths.get("MyFile3.txt");
    try(BufferedWriter writer = Files.newBufferedWriter(path, StandardOpenOption.APPEND)){
        writer.write("TITEL");
        writer.newLine();
        writer.write("Dit is een stuk tekst. Ik zit maar wat te brabbelen.");
        writer.newLine();
        writer.newLine();
        writer.write("Dit is een tweede alinea in het stuk tekst. Blablabla");
        writer.newLine();
        writer.write("Blablablabla");
    }catch(IOException ex){
        System.out.println("Ooops, something went wrong");
        System.out.println(ex.getMessage());
    }
}
}
