package be.kriscoolen.opdracht5;

import java.io.*;
import java.nio.charset.Charset;
import java.util.zip.Deflater;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.Inflater;
import java.util.zip.InflaterInputStream;

public class ToCompressedFile {

    public static void main(String[] args) {
        try {
            //schrijf "hello world" naar een gecomprimeerd bestand met naam 'myCompressedFile.txt'
            FileOutputStream fos = new FileOutputStream("myCompressedFile.txt");
            Deflater deflater = new Deflater(Deflater.DEFAULT_COMPRESSION);
            DeflaterOutputStream dos = new DeflaterOutputStream(fos,deflater);
            PrintStream ps = new PrintStream(dos,true, Charset.forName("UTF-8"));
            ps.println("Hello world!");
            ps.close();

            FileInputStream fis = new FileInputStream("myCompressedFile.txt");
            Inflater inf = new Inflater();
            InflaterInputStream  iis = new InflaterInputStream(fis,inf);
            InputStreamReader isr = new InputStreamReader(iis, Charset.forName("UTF-8"));
            try(BufferedReader reader = new BufferedReader(isr)){

                String line = null;
                while((line = reader.readLine())!=null){
                    System.out.println(line);
                }

            } catch (IOException ex) {
                System.err.println("Something happened while reading the file");
                System.err.println(ex.getMessage());
            }


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
