package be.kriscoolen.opdracht1;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.*;
import java.nio.file.attribute.DosFileAttributes;
import java.util.ArrayList;
import java.util.List;

public class DirectoryAndFileApp {
    public static void main(String[] args) {
        try{
            //define path
            Path path = Paths.get("C:\\Users\\Java01\\IdeaProjects\\h21lezenenschrijven\\folder\\file.txt");
            //create parent directories
            Files.createDirectories(path.getParent());
            //create if not exists
            if(Files.notExists(path)){
                Files.createFile(path);
            }
            else{
                System.out.println("File already exists.");
            }
            //Write lines of text to file
            List<String> lines = new ArrayList<>();
            lines.add("I rule");
            lines.add("the world");
            Files.write(path,lines, Charset.forName("UTF-8"),StandardOpenOption.APPEND);

            //Retrieve attributes of file
            DosFileAttributes attrs = Files.readAttributes(path,DosFileAttributes.class);

            System.out.println(attrs.size() + " bytes");
            System.out.println(attrs.creationTime());
            System.out.println(attrs.lastAccessTime());
            System.out.println(attrs.lastModifiedTime());
            System.out.println(attrs.isArchive());
            System.out.println(attrs.isHidden());
            System.out.println(attrs.isReadOnly());

            //read lines of text from file
            Files.lines(path).forEach(System.out::println);
            System.out.println("owner of file:");
            System.out.println(Files.getOwner(path).getName());
            //Rename file
            Path path2 = Paths.get("C:\\Users\\Java01\\IdeaProjects\\h21lezenenschrijven\\folder\\file_new_name.txt");
            //Files.createDirectories(path2.getParent());
           // if(Files.notExists(path2)){
           //     Files.createFile(path2);
           //     System.out.println("File created");
          //  }
          //  else{
           //     System.out.println("File already exists");
           // }
            if(Files.notExists(path2)){
                Files.move(path,path2);
            }
            else Files.move(path,path2,StandardCopyOption.REPLACE_EXISTING);


        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
